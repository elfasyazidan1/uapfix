package Contest;
//  class ini merupakan absbtract clasas yang berisi atribut dari tiket. pertama deklarasi atribut
//  setelah itu diberi setter dan getter agar atribut bisa dipanggil di class yang berbeda dan terakhir method hitunghargatiekt.
abstract class TiketKonser implements HargaTiket {
    private String nama;
    private double harga;

    public TiketKonser(String nama, double harga) {
        this.harga = harga;
        this.nama = nama;
    }
    public String getNama() {
        return nama;
    }
    public double getHarga() {
        return harga;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public void setHarga(double harga) {
        this.harga = harga;
    }
    public abstract double hitungHargaTiket();
}