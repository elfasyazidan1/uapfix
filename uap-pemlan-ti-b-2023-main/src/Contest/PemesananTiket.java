package Contest;
//  disini berisi data tiket atau ahrga harga tiket dengan disimpan pada array tiketkonser
class PemesananTiket {
    // Do your magic here...
    private static TiketKonser[] tiketKonser;

    static {
        tiketKonser = new TiketKonser[]{
            new CAT8("CAT 8", 10),
            new CAT1("CAT 1", 100),
            new FESTIVAL("FESTIVAL", 200),
            new VIP("VIP", 250),
            new VVIP("UNLIMITED EXPERIENCE", 300)
        };
    }

    public static TiketKonser pilihTiket(int index) {
        return tiketKonser[index];
    }
}