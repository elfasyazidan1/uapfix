package Contest;
//  class ini merupakan turuanan aatu child dari class tiketkonser. kita buat kontruktor dan panggil kelas supernya yaitu dari tiket konser setelah itu di override
//  hal ini sama juga pada jenis tiket yang lainnya jadi tidak usah dijelaskan lagi ya kakakku hehe
class CAT1 extends TiketKonser {
    public CAT1(String nama, double harga) {
        super(nama, harga);
    }
    @Override
    public double hitungHargaTiket(){
        return getHarga();
    }
}