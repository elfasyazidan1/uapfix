/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;
//  nah untuk class Main yang pertama kita inputkan dulu nama pemesan setelah itu tampilan menu
//  setelah itu input untuk memasukakn pilihan jenis tiket yang akan dipilih dan disini 
// juga menggunakan try and catch agar ketika terjadi error akan muncul masalah apa yang terjadi eror.
// contoh pada dibawah jika menginput angka diluar pilihan maka akan diperingatkan untuk memilih angka 1-5 seesuai pilihan
//setelah ituke class pemesanan tiket yang akan digunakan untuk menyesuaikan input pilihan yang dimasukkan tadi
// dengan tiket yang tersedia setelah itu untuk mengenerate kodebooking dant tanggal pesanan menggiunkan method yang sudah tersedia
//  setelah itu print struk seperti yang ada di contoh, dan untuk catch yang terakhir untuk defaultnya jika terjadi kesalahan, dari pada ada tulisan eror dari java yang tidak diketahui user diberi kata yang bisa dipahami user 
import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        //Do your magic here...
        boolean cek = true;
        Scanner scanner = new Scanner(System.in);
        int pilihan;

        System.out.println("Selamat datang di WAR Tiket Coldplay!");

        try {
            System.out.print("Masukkan nama pemesan: ");
            String namaPemesan = scanner.nextLine();
            
            System.out.println("Pilih jenis tiket:(angka)");
            
            System.out.println("1. CAT 8");
            System.out.println("2. CAT 1");
            System.out.println("3. FESTIVAL");
            System.out.println("4. VIP");
            System.out.println("5. UNLIMITED EXPERIENCE");

                try {
                    System.out.print("Masukkan pilihan: ");
                    pilihan = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException e) {
                    throw new InvalidInputException("pilih sesuai angka.");
                }

                if (pilihan < 1 || pilihan > 5) {
                    throw new InvalidInputException("Pilih angka 1-5.");
                } else if (pilihan >= 1 && pilihan <= 5) {
                    
                }
            
            TiketKonser tiket = PemesananTiket.pilihTiket(pilihan - 1);

            String kodeBooking = generateKodeBooking();

            String tanggalPesanan = getCurrentDate();

            System.out.println("\n----- Detail Pemesanan -----");
            System.out.println("Nama Pemesan: " + namaPemesan);
            System.out.println("Kode Booking: " + kodeBooking);
            System.out.println("Tanggal Pesanan: " + tanggalPesanan);
            System.out.println("Tiket yang dipesan: " + tiket.getNama());
            System.out.println("Total harga: " + tiket.hitungHargaTiket() + " USD");
        } catch (InvalidInputException e) {
            System.out.println("ERROR!! " + e.getMessage());
        } catch (Exception e) {
            System.out.println("ERROR!! " + e.getMessage());
        } finally {
            scanner.close();
        }
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}